# ED_NR

Elite: Dangerous offline neutron star plotter/router (WIP)

## Files

- `download.sh`: Download EDSM dumps (run this first)
- `process.py`: Generate systems.csv (run this second)
- `query.py`: Calculate route (WIP)

## Dependencies

- Python 3.7
- Numpy
- Scipy
- Pandas
- uJSON