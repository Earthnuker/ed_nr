import heapq
import numpy as np
import pandas as pd
from scipy.spatial import cKDTree
import sys
from functools import total_ordering
from datetime import datetime
import math
from hashlib import sha256
import pickle
import os
import gzip
import contextlib
import cProfile


@contextlib.contextmanager
def profile(filename="~/python.profile", *args, **kwargs):
    profile = cProfile.Profile(*args, **kwargs)
    profile.enable()
    yield
    profile.disable()
    if filename:
        profile.dump_stats(os.path.expanduser(filename))
    else:
        profile.print_stats()


class Load(object):
    "Display message+timing infos for code block"

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        self.t_start = datetime.today()
        print(*self.args, **self.kwargs, end="...\n", flush=True)

    def __exit__(self, *args, **kwargs):
        dt = datetime.today() - self.t_start
        print(*self.args, **self.kwargs, end=": done in {}\n".format(dt))


def dist(a, b):
    return np.sqrt(np.sum((a - b) ** 2, axis=0))


class Router:
    def __init__(self, filename, r):
        with open(filename, "rb") as fh, Load("Loading data"):
            self.tree, self.df, self.mults, self.valid = pickle.load(fh)
        self.r = r

    def neighbors(self, p, r):
        src = self.tree.data[p]
        for i in self.tree.query_ball_point(src, r=r):
            if i == p:
                continue
            yield i

    def cost(self, a, b):
        return dist(self.tree.data[a], self.tree.data[b])

    def length(self, idx, hops):
        cnt = 0
        while idx:
            idx = hops.get(idx)
            cnt += 1
        return cnt

    def route(self, src, dst, r=None):
        reached, idx, nodes = self.__route(src, dst, r)
        route = []
        while idx:
            route.insert(0, idx)
            idx = nodes.get(idx)
        route = self.df.iloc[route, :].copy()
        dist = (route[["x", "y", "z"]] - route.shift(-1)[["x", "y", "z"]]).to_numpy()
        route.loc[:, "jump_dist"] = np.sum(dist ** 2, axis=1) ** 0.5
        route.loc[:, "jump_dist"].fillna(0, inplace=True)
        del route["id"]
        return reached, route

    def __route(self, src, dst, r=None):
        if r is not None:
            self.r = r
        total_stars = len(self.df)
        print("Distance: {} Ly".format(dist(np.array(src), np.array(dst))))
        print("Jump Range: {} Ly".format(self.r))
        i_start = self.tree.query(src)[1]
        i_end = self.tree.query(dst)[1]
        cnt = 0
        d_prev = {i_start: None}
        Q = [(0, i_start, self.mults.get(i_start, 1.0))]
        while Q:
            cnt += 1
            depth, idx, mult = heapq.heappop(Q)
            r = self.r * mult
            if idx == i_end:
                print()
                print("Destination reached!")
                return True, idx, d_prev
            if cnt % 10000 == 0:
                print(
                    "Queue: {ql} Seen: {seen} ({prc:0.2%}) Depth: {depth} Dist: (Start: {d_start:0.2f}, End: {d_end:0.2f})".format(
                        ql=len(Q),
                        seen=len(d_prev),
                        prc=len(d_prev) / total_stars,
                        depth=depth,
                        d_start=self.cost(idx, i_start),
                        d_end=self.cost(idx, i_end),
                    ),
                    end="     \r",
                    flush=True,
                )
            for nb in self.neighbors(idx, r):
                if nb in d_prev:
                    continue
                if nb in self.valid or nb == i_end:
                    d_prev[nb] = idx
                    val = (depth + 1, nb, self.mults.get(nb, 1.0))
                    heapq.heappush(Q, val)
        print()
        print("Queue empty!")
        return False, None, None


def check(entry):
    "Filter jumps"
    return True
    first = entry.type.split()[0]
    return first == "Neutron" or first == "White" or first in "KGBFOAM"


def main(filename="stars.raw"):
    kdt = Router(filename, 48)
    with Load("Computing route"):
        src = (-65.21875, 7.75, -111.03125)  # Ix
        # dst = (-7095.375, 401.25, 2396.8125)  # V1357 Cygni
        # src = (-9530.5, 910.28125, 19808.125)  # Colonia
        # dst = (25.21875, -20.90625, 25899.96875)  # Sag A*
        dst = (-1111.5625, -134.21875, 65269.75)  # Beagle Point
        reached, route = kdt.route(src, dst, 48)
    if reached:
        print("Destination reached!")
    else:
        print("Destination unreachable!")
    print("{} Jumps, {} Ly total".format(len(route), route.loc[:, "jump_dist"].sum()))
    print(route)


if __name__ == "__main__":
    main()
